## React Native Pipeline

# What this script can do?

- It can create the build for production from the master branch and for staging from the staging branch


# How it can?

- First, it will check the branch name as we only want to create the build for the stanging and the master branch, if we don't check that it will create the build for all the branches

- Another thing we are checking here is the action, it should be an open merge request

- It is having 3 stages build, test, and deploy. You can update it as per your requirement.

- Under the build in will install all the required dependency 

- Under test it will check the lint, you can check the unit test cases as well

- Under deployment, it will create the build for the iOS and Android platforms, for now, you have to deploy the build manually as I have not added build deployment to the apple store and google play store yet


# Steps to follow,

- Install the git lab runner on the machine where you want to create the build
- https://docs.gitlab.com/runner/install/osx.html

- Install Fastlane to the machine
- https://docs.fastlane.tools/getting-started/ios/setup/

- Some of the variables used by the script,
<CI_COMMIT_AUTHOR> - The author of the commit in "Name <email>" format
<CI_PIPELINE_SOURCE> - How the pipeline was triggered?
<CI_COMMIT_BRANCH> - The commit branch name.
<CI_OPEN_MERGE_REQUESTS> - A comma-separated list of up to four merge requests that use the current branch and project as the merge request source. Only available in branch and merge request pipelines if the branch has an associated merge request.
<FASTLANE_CONFIGURATION> - To decide on build creation, it is for release or debugging?
<FASTLANE_EXPORT_METHOD> - For iOS only, The build is for the app-store, ad-hoc, etc
<FASTLANE_EXPORT_ENVIRONMENT> - For iOS only, Xcode target name

- Copy ".gitlab-ci.yml" to the root of your project and update the branch names, commit author, user name, user email, git ssh URL, and  Xcode target name

- Copy "fastlane" folder from "android/fastlane" folder to your android folder in react native project and updated the details(like package_name, build type) to Appfile and Fastfile files

- Copy "Gymfile" file from "ios" folder to your ios folder in react native project and updated the workspace name